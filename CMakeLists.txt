cmake_minimum_required(VERSION 3.7)
project(MultiChessboard)

#set(CMAKE_CXX_STANDARD 11)
set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS_DEBUG "$ENV{CXXFLAGS} -O0 -Wall -g -ggdb")
SET(CMAKE_CXX_FLAGS_RELEASE "$ENV{CXXFLAGS} -O3 -Wall")

find_package(OpenCV REQUIRED)
include_directories(include ${OpenCV_INCLUDE_DIRS})


set(SOURCE_FILES main.cpp CornerDetAC.cpp corealgmatlab.cpp CornerStructure.cpp CornerStructure.h)

add_executable(MultiChessboard ${SOURCE_FILES})
target_link_libraries(MultiChessboard ${OpenCV_LIBS})