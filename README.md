## MultiChessboard Corners Detection
- This is C++ code of realize paper [*Automatic Camera and Range Sensor Calibration using a single Shot*](https://pdfs.semanticscholar.org/140a/6bdfb0564eb18a1f51a39dff36f20272a461.pdf)

## References
1. [*Automatic Camera and Range Sensor Calibration using a single Shot*](https://pdfs.semanticscholar.org/140a/6bdfb0564eb18a1f51a39dff36f20272a461.pdf)
2. [*https://github.com/onlyliucat/Multi-chessboard-Corner-extraction-detection-*](https://github.com/onlyliucat/Multi-chessboard-Corner-extraction-detection-)
