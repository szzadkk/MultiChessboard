//
// Created by 闫明 on 2018/6/11.
//

#ifndef MULTICHESSBOARD_CORNERSTRUCTURE_H
#define MULTICHESSBOARD_CORNERSTRUCTURE_H
#include <opencv2/opencv.hpp>
#include <HeaderCB.h>

class CornerStructure {
public:
    CornerStructure();
    ~CornerStructure();
    /*!
     *
     * @param corners all detected corners
     * @param i corner indice
     * @param chessboard
     * @return if init success
     */
    bool initChessboard(Corners& corners,int i,cv::Mat& chessboard);

    /*!
     *
     * @param chessboard
     * @param corners all detect corners
     * @return
     */
    double chessboardEnergy(cv::Mat& chessboard,Corners& corners);

    /*!
     *
     * @param chessboard
     * @param corners all detect corners
     * @param border_type
     * @return
     */
    bool growChessboard(cv::Mat& chessboard,Corners& corners,int border_type,cv::Mat& chessboardOut);


    /*!
     *
     * @param idx
     * @param v
     * @param chessboard
     * @param corners
     * @return
     */
    double directionalNeighbor(int idx,
                               cv::Vec2f& v,
                               cv::Mat& chessboard,
                               Corners& corners,
                               int& neighbor_idx,
                               double& min_dist);

    /*!
     *
     * @param p1
     * @param p2
     * @param p3
     * @return
     */
    void predictCorners(cv::Mat& p1,cv::Mat& p2,cv::Mat p3,cv::Mat& pred);

   /*!
    *
    * @param cand
    * @param pred
    * @param idx
    * @return
    */
    bool assignClosestCorners(std::vector<cv::Point2f>& cand, cv::Mat& pred, std::vector<int>& idx);


    /*!
     *
     * @param corners
     */
    void chessboardsFromCorners(Corners& corners);

    const std::vector<cv::Mat>& getChessBoards() const;

private:
    std::vector<cv::Mat> chessboards;







};


#endif //MULTICHESSBOARD_CORNERSTRUCTURE_H
