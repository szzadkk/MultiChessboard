//
// Created by 闫明 on 2018/6/11.
//

#include "CornerStructure.h"
#include <numeric>

CornerStructure::CornerStructure() {

}

CornerStructure::~CornerStructure() {

}
template <class T>
bool findValue(cv::Mat &mat, T value) {
    for(int i = 0;i < mat.rows;i++) {
        const T* row = mat.ptr<T>(i);
        if(std::find(row, row + mat.cols, value) != row + mat.cols)
            return true;
    }
    return false;
}
void CornerStructure::chessboardsFromCorners(Corners &corners) {

    for(int i = 0;i<corners.p.size();i++) {
        cv::Mat chessboard;
        if (!initChessboard(corners, i, chessboard) || chessboardEnergy(chessboard, corners) > 0)
            continue;
        while (true) {
//            std::cout<<chessboard<<std::endl;
            double energy = chessboardEnergy(chessboard, corners);
            std::vector<cv::Mat> proposal;
            cv::Mat chessboardOut;
            std::vector<double> energy_v;
            int min_idx = -1;
            double min_val = std::numeric_limits<double>::max();
            for (int j = 0; j < 4; j++) {
                growChessboard(chessboard, corners, j, chessboardOut);
                proposal.push_back(chessboardOut);
                energy_v.push_back(chessboardEnergy(chessboardOut, corners));
//                std::cout<<chessboardOut<<std::endl;
                if (energy_v[j] < min_val) {
                    min_val = energy_v[j];
                    min_idx = j;
                }
            }
            if (energy_v[min_idx] < energy) {
                proposal[min_idx].copyTo(chessboard);
            } else break;
        }
        if (chessboardEnergy(chessboard, corners) < -10) {
            cv::Mat overlap = cv::Mat::zeros(chessboards.size(), 2, CV_32F);
            bool find = false;
            for (int j = 0; j < chessboards.size(); j++) {
                for (int k = 0; k < chessboards[j].rows; k++) {
                    for (int m = 0; m < chessboards[j].cols; m++) {
                        //修复bug,CV_32S类型取的double变量
                        if (findValue(chessboard, chessboards[j].at<int>(k, m))) {
                            find = true;
                            overlap.at<float>(j, 0) = 1;
                            overlap.at<float>(j, 1) = static_cast<float>(chessboardEnergy(chessboards[j], corners));
                            break;
                        }
                        if (find)
                            break;
                    }
                }
                find = false;
            }
            std::vector<int> idx;
            for (int i = 0; i < overlap.rows; i++) {
                if (fabs(overlap.at<float>(i, 0) - 1) < 0.0001) {
                    idx.push_back(i);
                }
            }
            if (idx.empty())
                chessboards.push_back(chessboard);
            else {
                bool overlapflag = false;
                for (int i = 0; i < idx.size(); i++) {
                    if (overlap.at<float>(idx[i], 1) <= chessboardEnergy(chessboard, corners)) {
                        overlapflag = true;
                        break;
                    }
                }
                if (!overlapflag) {
                    for (std::vector<int>::reverse_iterator riter = idx.rbegin(); riter != idx.rend(); riter++) {
                        std::vector<cv::Mat>::iterator iter = chessboards.begin();
                        chessboards.erase(iter + *riter);
                    }
                    chessboards.push_back(chessboard);
                }
            }

        }
    }
}
bool CornerStructure::initChessboard(Corners &corners, int i, cv::Mat &chessboard) {
    if(corners.p.size() < 9)
        return false;
    chessboard = cv::Mat::zeros(3,3,CV_32S)-1;

    cv::Vec2f v1 = corners.v1[i];
    cv::Vec2f v2 = corners.v2[i];
    cv::Vec2f nv1;
    cv::Vec2f nv2;
    nv1[0] = -v1[0];
    nv1[1] = -v1[1];
    nv2[0] = -v2[0];
    nv2[1] = -v2[1];

    chessboard.at<int>(1,1) = i;

    int neighbor_idx = -1;
    double min_dist = std::numeric_limits<double>::max();

    cv::Mat dist1 = cv::Mat::zeros(2,1,CV_64F);
    cv::Mat dist2 = cv::Mat::zeros(6,1,CV_64F);

    directionalNeighbor(i,v1,chessboard,corners,neighbor_idx,min_dist);
    chessboard.at<int>(1,2) = neighbor_idx;
    dist1.at<double>(0) = min_dist;

    directionalNeighbor(i,nv1,chessboard,corners,neighbor_idx,min_dist);
    chessboard.at<int>(1,0) = neighbor_idx;
    dist1.at<double>(1) = min_dist;

    directionalNeighbor(i,v2,chessboard,corners,neighbor_idx,min_dist);
    chessboard.at<int>(2,1) = neighbor_idx;
    dist2.at<double>(0)  = min_dist;

    directionalNeighbor(i,nv2,chessboard,corners,neighbor_idx,min_dist);
    chessboard.at<int>(0,1) = neighbor_idx;
    dist2.at<double>(1)  = min_dist;

    directionalNeighbor(chessboard.at<int>(1,0),nv2,chessboard,corners,neighbor_idx,min_dist);
    chessboard.at<int>(0,0) = neighbor_idx;
    dist2.at<double>(2)= min_dist;

    directionalNeighbor(chessboard.at<int>(1,0),v2,chessboard,corners,neighbor_idx,min_dist);
    chessboard.at<int>(2,0) = neighbor_idx;
    dist2.at<double>(3) = min_dist;

    directionalNeighbor(chessboard.at<int>(1,2),nv2,chessboard,corners,neighbor_idx,min_dist);
    chessboard.at<int>(0,2) = neighbor_idx;
    dist2.at<double>(4) = min_dist;

    directionalNeighbor(chessboard.at<int>(1,2),v2,chessboard,corners,neighbor_idx,min_dist);
    chessboard.at<int>(2,2) = neighbor_idx;
    dist2.at<double>(5) = min_dist;

//    std::cout<<chessboard<<std::endl;
//    std::cout<<dist1<<std::endl;
//    std::cout<<dist2<<std::endl;


    for(int j = 0;j<2;j++)
    {
        if(std::numeric_limits<double>::max()-dist1.at<double>(j) < 0.0001)
            return false;
    }
    for(int j = 0;j<6;j++)
    {
        if(std::numeric_limits<double>::max()-dist2.at<double>(j) < 0.0001)
            return false;
    }

    cv::Mat dist1mean;
    cv::Mat dist1std;
    cv::Mat dist2mean;
    cv::Mat dist2std;
    cv::meanStdDev(dist1,dist1mean,dist1std);
    cv::meanStdDev(dist2,dist2mean,dist2std);

//    std::cout<<dist1mean<<std::endl;
//    std::cout<<dist1std<<std::endl;

    if(dist1std.at<double>(0)/dist1mean.at<double>(0) > 0.3 || dist2std.at<double>(0)/dist2mean.at<double>(0) > 0.3)
        return false;
    return true;


}
double CornerStructure::directionalNeighbor(int idx, cv::Vec2f &v, cv::Mat &chessboard, Corners &corners,
                                            int &neighbor_idx, double &min_dist) {
    std::vector<int> unused;
    std::vector<int> used;
    for(int i = 0;i<chessboard.rows;i++)
        for(int j = 0;j<chessboard.cols;j++)
        {
            int tmp = chessboard.at<int>(i,j);
            if(tmp != -1)
                used.push_back(tmp);
        }


    for(int i = 0;i<corners.p.size();i++)
    {
        if(std::find(used.begin(),used.end(),i) == used.end())
            unused.push_back(i);
    }


    int n = static_cast<int>(unused.size());
    cv::Mat matp(n,2,CV_32F);
    cv::Mat idxp(1,2,CV_32F);
    idxp.at<float>(0) = corners.p[idx].x;
    idxp.at<float>(1) = corners.p[idx].y;
    for(int i = 0;i<n;i++)
    {
        matp.at<float>(i,0) = corners.p[unused[i]].x;
        matp.at<float>(i,1) = corners.p[unused[i]].y;
    }


    cv::Mat dir = matp - cv::Mat::ones(n,1,CV_32F)*idxp;
    cv::Mat dist = dir.col(0)*v[0] + dir.col(1)*v[1];

    cv::Mat vmat(1,2,CV_32F);
    vmat.at<float>(0) = v[0];
    vmat.at<float>(1) = v[1];
    cv::Mat dist_edge = dir - dist*vmat;
    cv::sqrt(dist_edge.col(0).mul(dist_edge.col(0))+dist_edge.col(1).mul(dist_edge.col(1)),dist_edge);
//    cv::Mat pow2dist0;
//    cv::pow(dist_edge.col(0),2,pow2dist0);
//    cv::Mat pow2dist1;
//    cv::pow(dist_edge.col(1),2,pow2dist1);
//    cv::sqrt(pow2dist0+pow2dist1,dist_edge);
    cv::Mat dist_point;
    dist.copyTo(dist_point);
    for(int i = 0;i<dist_point.rows;i++)
        for(int j = 0;j<dist_point.cols;j++)
        {
            if(dist_point.at<float>(i,j) < 0)
                dist_point.at<float>(i,j) = std::numeric_limits<float>::max();
        }

    cv::Point minloc;
    cv::minMaxLoc(dist_point+5*dist_edge,&min_dist,NULL,&minloc,NULL);
    neighbor_idx = unused[minloc.y];
}
double CornerStructure::chessboardEnergy(cv::Mat &chessboard, Corners &corners) {

    int E_corners = -chessboard.rows*chessboard.cols;
    double E_structure = 0;

    for(int j = 0;j<chessboard.rows;j++)
        for(int k = 0;k<chessboard.cols-2;k++)
        {
            cv::Mat x(3,2,CV_32F);
            x.at<float>(0,0) = corners.p[chessboard.at<int>(j,k)].x;
            x.at<float>(0,1) = corners.p[chessboard.at<int>(j,k)].y;
            x.at<float>(1,0) = corners.p[chessboard.at<int>(j,k+1)].x;
            x.at<float>(1,1) = corners.p[chessboard.at<int>(j,k+1)].y;
            x.at<float>(2,0) = corners.p[chessboard.at<int>(j,k+2)].x;
            x.at<float>(2,1) = corners.p[chessboard.at<int>(j,k+2)].y;


            double tmp =  cv::norm(x.row(0)+x.row(2)-2*x.row(1))/cv::norm(x.row(0)-x.row(2));
            E_structure = E_structure > tmp ? E_structure : tmp;
        }

    for(int j = 0;j<chessboard.cols;j++)
        for(int k = 0;k<chessboard.rows-2;k++)
        {
            cv::Mat x(3,2,CV_32F);
            x.at<float>(0,0) = corners.p[chessboard.at<int>(k,j)].x;
            x.at<float>(0,1) = corners.p[chessboard.at<int>(k,j)].y;
            x.at<float>(1,0) = corners.p[chessboard.at<int>(k+1,j)].x;
            x.at<float>(1,1) = corners.p[chessboard.at<int>(k+1,j)].y;
            x.at<float>(2,0) = corners.p[chessboard.at<int>(k+2,j)].x;
            x.at<float>(2,1) = corners.p[chessboard.at<int>(k+2,j)].y;

            double tmp =  cv::norm(x.row(0)+x.row(2)-2*x.row(1))/cv::norm(x.row(0)-x.row(2));
            E_structure = E_structure > tmp ? E_structure : tmp;
        }

    double E = E_corners + chessboard.rows*chessboard.cols*E_structure;
    return E;


}
bool CornerStructure::growChessboard(cv::Mat &chessboard, Corners &corners, int border_type, cv::Mat& chessboardOut) {
    if(chessboard.empty())
        return false;

    std::vector<cv::Point2f> p(corners.p);
    std::vector<int> unused;
    std::vector<int> used;
    for(int i = 0;i<chessboard.rows;i++)
        for(int j = 0;j<chessboard.cols;j++)
        {
            int tmp = chessboard.at<int>(i,j);
            if(tmp != -1)
                used.push_back(tmp);
        }

    for(int i = 0;i<corners.p.size();i++)
    {
        if(std::find(used.begin(),used.end(),i) == used.end())
            unused.push_back(i);
    }

    std::vector<cv::Point2f> cand;
    for(int i = 0;i<unused.size();i++)
    {
        cand.push_back(p[unused[i]]);
    }

    switch (border_type)
    {
        case 0:
        {
            cv::Mat p1(chessboard.rows,2,CV_32F);
            cv::Mat p2(chessboard.rows,2,CV_32F);
            cv::Mat p3(chessboard.rows,2,CV_32F);
            for(int i = 0;i<chessboard.rows;i++)
            {
                p1.at<float>(i,0) = p[chessboard.at<int>(i,chessboard.cols-3)].x;
                p1.at<float>(i,1) = p[chessboard.at<int>(i,chessboard.cols-3)].y;
                p2.at<float>(i,0) = p[chessboard.at<int>(i,chessboard.cols-2)].x;
                p2.at<float>(i,1) = p[chessboard.at<int>(i,chessboard.cols-2)].y;
                p3.at<float>(i,0) = p[chessboard.at<int>(i,chessboard.cols-1)].x;
                p3.at<float>(i,1) = p[chessboard.at<int>(i,chessboard.cols-1)].y;
            }
            cv::Mat pred;
            predictCorners(p1,p2,p3,pred);

            std::vector<int> idx;
            if(assignClosestCorners(cand,pred,idx))
            {
                cv::Mat unusedmat = cv::Mat(idx.size(),1,CV_32S);
                for(int i = 0;i<idx.size();i++)
                {
                    unusedmat.at<int>(i) = unused[idx[i]];
                }
                cv::hconcat(chessboard,unusedmat,chessboardOut);
            }
        }
            break;
        case 1:
        {
            cv::Mat p1(chessboard.cols,2,CV_32F);
            cv::Mat p2(chessboard.cols,2,CV_32F);
            cv::Mat p3(chessboard.cols,2,CV_32F);
            for(int i = 0;i<chessboard.cols;i++)
            {
                p1.at<float>(i,0) = p[chessboard.at<int>(chessboard.rows-3,i)].x;
                p1.at<float>(i,1) = p[chessboard.at<int>(chessboard.rows-3,i)].y;
                p2.at<float>(i,0) = p[chessboard.at<int>(chessboard.rows-2,i)].x;
                p2.at<float>(i,1) = p[chessboard.at<int>(chessboard.rows-2,i)].y;
                p3.at<float>(i,0) = p[chessboard.at<int>(chessboard.rows-1,i)].x;
                p3.at<float>(i,1) = p[chessboard.at<int>(chessboard.rows-1,i)].y;
            }
//            std::cout<<p1<<std::endl;
//            std::cout<<p2<<std::endl;
//            std::cout<<p3<<std::endl;
            cv::Mat pred;
            predictCorners(p1,p2,p3,pred);
//            std::cout<<pred<<std::endl;
            std::vector<int> idx;
            if(assignClosestCorners(cand,pred,idx))
            {
                cv::Mat unusedmat = cv::Mat(1,idx.size(),CV_32S);
                for(int i = 0;i<idx.size();i++)
                {
                    unusedmat.at<int>(i) = unused[idx[i]];
                }
                cv::vconcat(chessboard,unusedmat,chessboardOut);
            }
        }
            break;
        case 2:
        {
            cv::Mat p1(chessboard.rows,2,CV_32F);
            cv::Mat p2(chessboard.rows,2,CV_32F);
            cv::Mat p3(chessboard.rows,2,CV_32F);
            for(int i = 0;i<chessboard.rows;i++)
            {
                p1.at<float>(i,0) = p[chessboard.at<int>(i,2)].x;
                p1.at<float>(i,1) = p[chessboard.at<int>(i,2)].y;
                p2.at<float>(i,0) = p[chessboard.at<int>(i,1)].x;
                p2.at<float>(i,1) = p[chessboard.at<int>(i,1)].y;
                p3.at<float>(i,0) = p[chessboard.at<int>(i,0)].x;
                p3.at<float>(i,1) = p[chessboard.at<int>(i,0)].y;
            }
            cv::Mat pred;
            predictCorners(p1,p2,p3,pred);
            std::vector<int> idx;
            if(assignClosestCorners(cand,pred,idx))
            {
                cv::Mat unusedmat = cv::Mat(idx.size(),1,CV_32S);
                for(int i = 0;i<idx.size();i++)
                {
                    unusedmat.at<int>(i) = unused[idx[i]];
                }
                cv::hconcat(unusedmat,chessboard,chessboardOut);
            }
        }
            break;
        case 3:
        {
            cv::Mat p1(chessboard.cols,2,CV_32F);
            cv::Mat p2(chessboard.cols,2,CV_32F);
            cv::Mat p3(chessboard.cols,2,CV_32F);
            for(int i = 0;i<chessboard.cols;i++)
            {
                p1.at<float>(i,0) = p[chessboard.at<int>(2,i)].x;
                p1.at<float>(i,1) = p[chessboard.at<int>(2,i)].y;
                p2.at<float>(i,0) = p[chessboard.at<int>(1,i)].x;
                p2.at<float>(i,1) = p[chessboard.at<int>(1,i)].y;
                p3.at<float>(i,0) = p[chessboard.at<int>(0,i)].x;
                p3.at<float>(i,1) = p[chessboard.at<int>(0,i)].y;
            }
            cv::Mat pred;
            predictCorners(p1,p2,p3,pred);
            std::vector<int> idx;
            if(assignClosestCorners(cand,pred,idx))
            {
                cv::Mat unusedmat = cv::Mat(1,idx.size(),CV_32S);
                for(int i = 0;i<idx.size();i++)
                {
                    unusedmat.at<int>(i) = unused[idx[i]];
                }
                cv::vconcat(unusedmat,chessboard,chessboardOut);
            }
        }
            break;
        default:
            break;
    }

}

void CornerStructure::predictCorners(cv::Mat &p1, cv::Mat &p2, cv::Mat p3, cv::Mat &pred) {
    cv::Mat v1 = p2-p1;
    cv::Mat v2 = p3-p2;

    cv::Mat a1(v1.rows,1,CV_32F);
    cv::Mat a2(v1.rows,1,CV_32F);
    for(int i = 0;i<v1.rows;i++)
    {
        a1.at<float>(i) = atan2(v1.at<float>(i,1),v1.at<float>(i,0));
        a2.at<float>(i) = atan2(v2.at<float>(i,1),v2.at<float>(i,0));
    }
//    std::cout<<"a1: "<<a1<<std::endl;
//    std::cout<<"a2: "<<a2<<std::endl;
    cv::Mat a3 = 2*a2-a1;
    cv::Mat anglea3(a3.rows,2,CV_32F);
    for(int i = 0;i<anglea3.rows;i++)
    {
        anglea3.at<float>(i,0) = std::cos(a3.at<float>(i));
        anglea3.at<float>(i,1) = std::sin(a3.at<float>(i));

    }


    cv::Mat s1,s2;
    cv::sqrt(v1.col(0).mul(v1.col(0))+v1.col(1).mul(v1.col(1)),s1);
    cv::sqrt(v2.col(0).mul(v2.col(0))+v2.col(1).mul(v2.col(1)),s2);

//    std::cout<<"s1: "<<s1<<std::endl;
//    std::cout<<"s2: "<<s2<<std::endl;

    cv::Mat s3 = 2*s2-s1;

    pred = p3+(0.75*s3*cv::Mat::ones(1,2,CV_32F)).mul(anglea3);

}

bool CornerStructure::assignClosestCorners(std::vector<cv::Point2f> &cand, cv::Mat &pred, std::vector<int>& idx) {
    if(cand.size() < pred.rows)
    {
        idx.resize(1,0);
        return false;
    }

    cv::Mat candmat = cv::Mat::zeros(cand.size(),2,CV_32F);
    for(int i = 0;i<cand.size();i++)
    {
        candmat.at<float>(i,0) = cand[i].x;
        candmat.at<float>(i,1) = cand[i].y;
    }
    cv::Mat D = cv::Mat::zeros(candmat.rows,pred.rows,CV_32F);
    for(int i = 0;i<pred.rows;i++)
    {
        cv::Mat delta = candmat - cv::Mat::ones(candmat.rows,1,CV_32F)*pred.row(i);
        cv::sqrt(delta.col(0).mul(delta.col(0))+delta.col(1).mul(delta.col(1)),D.col(i));
    }
//    std::cout<<D<<std::endl;

    std::vector<int> minrowvec;
    std::vector<int> mincolvec;

    for(int i = 0;i<pred.rows;i++)
    {
        float mind = std::numeric_limits<float>::max();
        int minrow = 0;
        int mincol = 0;
        for(int j = 0;j<D.rows;j++)
            for(int k = 0;k<D.cols;k++)
            {
                if(D.at<float>(j,k) < mind)
                {
                    minrow = j;
                    mincol = k;
                    mind = D.at<float>(j,k);
                }
            }
        D.row(minrow) = cv::Mat::ones(1,D.cols,CV_32F)*std::numeric_limits<float>::max();
        D.col(mincol) = cv::Mat::ones(D.rows,1,CV_32F)*std::numeric_limits<float>::max();
        minrowvec.push_back(minrow);
        mincolvec.push_back(mincol);
    }

    std::vector<int>::iterator citer = std::max_element(mincolvec.begin(),mincolvec.end()),riter;
    idx.resize(*citer+1,0);
    for(citer = mincolvec.begin(),riter = minrowvec.begin();citer != mincolvec.end() && riter != minrowvec.end();++citer,++riter)
    {
        idx[*citer] = *riter;
    }
    return true;
}
const std::vector<cv::Mat>& CornerStructure::getChessBoards() const {
    return chessboards;
}