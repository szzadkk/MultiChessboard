/*  Copyright 2017 only(997737609@qq.com). All Rights Reserved.           */
/*                                                                        */
/*  This file is part of ACRC(自动多棋盘格提取).                          */
/*                                                                        */
/*  part of source code come from https://github.com/qibao77/cornerDetect */
/*  Automatic Camera and Range Sensor Calibration using a single Shot     */
/*  this project realize the papar: Automatic Camera and Range Sensor     */
/*  Calibration using a single Shot                                       */
/*  the chess recovery class source code can contact 997737609@qq.com     */

#include "opencv2/opencv.hpp"

#include <algorithm>
#include "CornerDetAC.h"
#include <HeaderCB.h>

#include <stdio.h>
#include <iostream>
#include <time.h>

#include <fstream>

#include "CornerStructure.h"

using namespace cv;
using namespace std;
using std::vector;
vector<Point2i> points;

int main(int argc, char* argv[])
{

	Mat src1; 
	cv::Mat src;

	printf("read file...\n");

	string sr = "../02";

	string simage, stxt, ssave;
	simage = sr + ".png";
	stxt = sr + ".txt";
	ssave = sr + ".png";

	src1 = imread(simage.c_str(), -1);
	if (src1.channels() == 1)
	{
		src = src1.clone();
	}
	else
	{
		if (src1.channels() == 3)
		{
			cv::cvtColor(src1, src, CV_BGR2GRAY);
		}
		else
		{
			if (src1.channels() == 4)
			{
				cv::cvtColor(src1, src, CV_BGRA2GRAY);
			}
		}
	}

	if (src.empty())//不能读取图像
	{
		printf("Cannot read image file: %s\n", simage.c_str());
		return -1;
	}
	else
	{
		printf("read image file ok\n");
	}

	vector<Point> corners_p;//存储找到的角点
	
//	double t = (double)getTickCount();
	
	CornerDetAC corner_detector(src);

	Corners corners_s;
	corner_detector.detectCorners(src, corners_p, corners_s, 0.01);

//    std::fstream fpoint;
//    std::fstream fv1;
//    std::fstream fv2;
//    std::fstream fscore;
//    fpoint.open("../point.txt",ios::out);
//    fv1.open("../v1.txt",ios::out);
//    fv2.open("../v2.txt",ios::out);
//    fscore.open("../score.txt",ios::out);
//    if(fpoint.is_open())
//    {
//        for(const auto& point : corners_s.p)
//        {
//            fpoint<<point.x<<" "<<point.y<<endl;
//        }
//    }
//
//    for(const auto& v1 : corners_s.v1)
//    {
//        fv1<<v1[0]<<" "<<v1[1]<<endl;
//    }
//    for(const auto& v2 : corners_s.v2)
//    {
//        fv2<<v2[0]<<" "<<v2[1]<<endl;
//    }
//    for(const auto& score : corners_s.score)
//    {
//        fscore<<score<<endl;
//    }
//    fpoint.close();
//    fv1.close();
//    fv2.close();
//    fscore.close();




	CornerStructure corner_structure;
	corner_structure.chessboardsFromCorners(corners_s);

	std::vector<cv::Mat> chessboards = corner_structure.getChessBoards();

    src1 = imread(simage.c_str(), -1);
	for(int i = 0;i<chessboards.size();i++)
    {
        cv::Scalar color(rand()%256,rand()%256,rand()%256);
        for(int j = 0;j<chessboards[i].rows;j++)
        {

            for(int k = 0;k<chessboards[i].cols;k++)
            {
                cv::circle(src1,Point(corners_s.p[chessboards[i].at<int>(j,k)].x,corners_s.p[chessboards[i].at<int>(j,k)].y),2,color);
            }
        }
    }
    cv::imshow("image",src1);
	cv::waitKey(0);


	//corner_detector.savecorners(corners_s, "corner.txt");
	//corner_detector.readcorners(corners_s, "corner.txt");

	// t = ((double)getTickCount() - t) / getTickFrequency();
	// std::cout << "time cost :" << t << std::endl;
	// ImageChessesStruct ics;
	// std::vector<cv::Mat> chessboards;
	// ChessboardRecovery cbr;
	// cbr.ChessboardRecoveryRun(src1, corners_s, chessboards, ics, true);


	return 0;
}

